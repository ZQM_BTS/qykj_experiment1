## <p align = "center"><b>东莞理工学院网络空间安全学院</b> </p>
#### <p align = "center"><b>实验报告模板</b> </p>
<span><b>课程名称：企业级开发框架专题</b></span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
<span><b>学期：2020春季</b></span><br/><br/>

`实验名称`：使用Spring Boot构建应用程序&emsp;&emsp;&emsp;&emsp;`实验序号`：一  
`姓名`：张琼敏&emsp;&emsp;&emsp;&emsp;`学号`：201741412229&emsp;&emsp;&emsp;&emsp;`班级`：17软卓2班  
`实验地址`：居家&emsp;&emsp;&emsp;&emsp;`实验日期`：2020-3-25&emsp;&emsp;&emsp;&emsp;`指导老师`：黎志雄  
`教师评语`：XXX&emsp;&emsp;&emsp;&emsp;`实验成绩`：XXX&emsp;&emsp;&emsp;&emsp;`百分制`：XXX  
`同组同学`：无  

<a name="cntitle"></a>
#### 一、实验目标
1、掌握使用IntelliJ IDEA创建Spring Boot应用程序的方法；  
2、了解spring-boot-starter-parent的配置内容；  
3、掌握如何利用Starter扩展Spring Boot应用程序的功能；  
4、掌握如何配置Starter；  
5、掌握如何通过属性文件定制Spring Boot应用程序的初始化参数；  
6、掌握使用Spring Boot编写简单的单元测试；  
7、了解Spring Boot应用程序的Fat Jar文件；  
8、掌握Markdown轻量级标记语言编写README.md文件。  

<a name="cntitle"></a>
#### 二、实验条件
1、JDK1.8或更高版本  
2、Maven3.6+  
3、IntelliJ-IDEA  

<a name="cntitle"></a>
#### 三、实验内容与实验步骤
##### 1、通过IntelliJ IDEA的Spring Initializr向导创建Spring Boot项目；  
#### `步骤`：
File->new Project->选择Spring Initializr->next->填写相关信息->选择需要的starter->next,具体如下图：  

<div align=center>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/173528_ac11c6a3_4840371.png "创建1.png""/>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/174033_644d4df7_4840371.png "创建2.png""/>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/174047_12a78471_4840371.png "创建3.png""/>
</div>  

##### 2、添加两个功能模块：spring MVC、lombok；
#### `步骤`：  
1.由于通过IntelliJ-IDEA的Spring-Initializr向导创建Spring-Boot项目时，在pom.xml文件中已经有parent标签（其中包含了springboot大部分的依赖插件），如下图：
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/180515_5b15c494_4840371.png "parent.png"/>
</div> 

2.spring MVC功能模板也在创建Spring Boot项目时即已经默认配置好相关依赖了，如下图：
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/180601_6cd025f3_4840371.png "springmvc功能模板.png""/>
</div> 

3.添加lombok功能模板，在pom.xml中添加
```
  <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
  </dependency>
```
创建一个利用lombok的类进行测试，如下：
```  
public class App {
    public static void main(String[] args) {
        System.out.println("hello springboot!!");
        Student s = Student.builder().name("张琼敏Jonmin").age(21).build();
        System.out.println(s);
    }

    @Data
    @Builder
    static class Student {
        String name;
        int age;
    }
}
```

##### 3、添加阿里云镜像仓库作为项目maven仓库；
#### `步骤`：
在项目所用的仓库所在目录的settings.xml中添加
```
<mirror>
      <id>USTC</id>
      <name>中科大反向代理</name>
      <url>https://maven.proxy.ustclug.org/maven2/</url>
      <mirrorOf>central</mirrorOf>
</mirror>
```
具体如下图：
<div align=center>
    <img width = '40%' src ="https://images.gitee.com/uploads/images/2020/0325/181607_b474cbd3_4840371.png "设置阿里云仓库.png""/>
</div>

##### 4、解释项目pom.xml文件中主要标签的意义；   
 ```
project: pom.xml文件中的顶层元素；  
modelVersion: 指明POM使用的对象模型的版本;  
groupId: 指明创建项目的组织或者小组的唯一标识。GroupId是项目的关键标识，典型的，此标识以组织的完全限定名来定义。  
artifactId: 指明此项目产生的主要产品的基本名称。项目的主要产品通常为一个JAR文件。  
version: 项目产品的版本号。Maven帮助你管理版本，可以经常看到SNAPSHOT这个版本，表明项目处于开发阶段。    
name: 项目的显示名称，通常用于maven产生的文档中。  
description: 描述此项目，通常用于maven产生的文档中。  
properties: pom文件中的配置信息，可以配置全局变量。   
ependencies: 依赖配置集，里面可以添加需要的jar的依赖信息。    
parent: 父项目的坐标。如果项目中没有规定某个元素的值，那么父项目中的对应值即为项目的默认值。  
relativePath: 父项目的pom.xml文件的相对路径。相对路径允许你选择一个不同的路径。默认值是../pom.xml。Maven首先在构建当前项目的地方寻找父项 目的pom，其次在文件系统的这个位置（relativePath位置），然后在本地仓库，最后在远程仓库寻找父项目的pom。  
extensions: 使用来自该项目的一系列构建扩展。  
plugins: 使用的插件列表。  
repositories: 发现依赖和扩展的远程仓库列表。  
```
##### 5、配置jetty或undertow作为Spring-Boot应用程序的默认Servlet容器；   
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/203656_78e05674_4840371.png "默认tomcat.png""/>
</div>

#### `步骤`（这里是配置jetty):  
由上图可知，Tomcat 是默认的嵌入式 Web 服务器容器。现在我们假设您不想使用 Tomcat，而是想使用 Jetty。只需更改pom.xml中的 <dependencies> ：
```
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-tomcat</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jetty</artifactId>
    </dependency>
</dependencies>

```

##### 6、添加一个简单的Spring-Mvc控制器组件，用于测试；  
具体如下：  
```
@SpringBootApplication
@RestController

public class Experiment1Application {

    public static void main(String[] args) {
        SpringApplication.run(Experiment1Application.class, args);
    }
    @RequestMapping("/")
    public String home() {
        return "这是一个简单的Spring Mvc控制器组件！";
    }
}
``` 
 
#### `说明`：  
该类被标记为@RestController，这意味着Spring MVC可以使用它来处理Web请求。@RequestMapping映射‘/’到该类的index()方法。从浏览器调用或在命令行上使用curl时，该方法返回纯文本。那是因为@RestController是复合注解，它将@Controller和@ResponseBody组合在一起，这两个注解会使Spring Mvc处理Web请求返回的是数据而不是视图。

##### 7、定义一个CommandLineRunner的Bean，用于检查Spring-Boot应用程序启动完成后在Spring-IoC容器中注册的所有Bean；  
具体如下：
```
    @Bean
    public CommandLineRunner commandLineRunner3 (ApplicationContext ctx) {
        return args -> {
            System.out.println("由Spring Boot注册的所有Bean: ");
            String[] beanNames = ctx . getBeanDefinitionNames();
            Arrays. sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
        };
    }
```

#### `说明`：  
Spring Boot应用程序初始化完成后，会自动回调所有CommandLineRunner类型的Bean的run方法。
实验中，我们自定义一个CommandLineRunner，检查由您的应用程序创建或由Spring Boot自动添加的所有bean，并对它们进行排序并打印出来。  

##### 8、编写一个简单的单元测试；
具体如下：
<div align=center>
    <img width = '70%' src ="https://images.gitee.com/uploads/images/2020/0325/205005_30e23d78_4840371.png "单元测试.png""/>
</div> 

```
说明：  MockMvc来自Spring Test模块，它可以让您通过一组便捷的构建器类将HTTP请求发送到DispatcherServlet中，并对结果进行断言。
注意，这里使用@AutoConfigureMockMvc和@SpringBootTest。
1）@SpringBootTest
使用完@SpringBootTest后，将创建整个应用程序上下文(加载所有配置类的Bean)。
@SpringBootTest注解告诉SpringBoot去寻找一个主配置类(例如带有@SpringBootApplication的配置类)，并使用它来启动Spring应用程序上下文。SpringBootTest加载完整的应用程序并注入所有可能的bean，因此速度会很慢。
2）@AutoConfigureMockMvc
Spring在该层处理传入的HTTP请求并将其交给您的控制器。这样，几乎使用了整个堆栈，并且将以与处理真实HTTP请求完全相同的方式调用您的代码，而无需启动Servlet容器就可以进行测试。使用@AutoConfigureMockMvc注解后，则会在spring应用上下文创建MockMvc实例。在测试用例中可以注入MockMvc实例模拟http请求，MockMvc实例会把http请求转交相应的controller处理，而且不需要启动tomcat等Servlet容器。
```
##### 9、使用IntelliJ-IDEA的HTTP-Client工具测试控制器端口；  
#### `说明`：
在项目目录中生成一个以 .http 为扩展名的文件，IntelliJ IDEA会自动识别为http脚本，然后编写脚本进行调试。
#### `步骤`：
1.建立.http为扩展名的文件；  
<div align=center>
    <img width = '30%' src ="https://images.gitee.com/uploads/images/2020/0325/211143_0eb2ed7c_4840371.png "http建立.png""/>
</div> 

2.编写脚本写法，具体如下：
```
GET http://localhost:8080/(这里控制类的Mapping为"/")
###
POST http://localhost:8080/post(这里控制类的Mapping为"/post")
###
POST http://localhost:8080/Map(这里控制类的Mapping为"/Map")
Content-Type: application/json

{
  "id": 999,
  "value": "content"
}
###
```

##### 10、在命令行中使用spring-boot插件运行Spring-Boot应用程序，并把嵌入式Servlet容器的默认端口8080改为9090；  
```
使用spring-boot插件运行Spring-Boot应用程序的命令行:   
mvn spring-boot:run
同时将嵌入式Servlet容器的默认端口8080改为9090的命令行：   
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dserver.port=9090"
```
##### 11、在属性文件中配置Spring-Boot应用程序以debug模式运行；
```
命令行： mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Ddebug"
```
##### 12、在命令行中编译、打包Spring-Boot应用程序；
```
 命令行（跳过test）： mvn clean package -DskipTests
```
##### 13、在命令行中使用java命令运行Spring-Boot应用程序的Jar文件；
```
命令行： java -jar  experiment1-0.0.1-SNAPSHOT.jar
说明：  experiment1-0.0.1-SNAPSHOT.jar为应用程序打包后的jar文件
```
##### 14、在命令行中使用java命令运行Spring-Boot应用程序的Jar文件，带参数改变嵌入式Servlet容器的默认端口8080改为9090；
```
命令行： java -jar  experiment1-0.0.1-SNAPSHOT.jar --server.port=9090
```
##### 15、使用markdown标记语方编写实验报告，并生成README.md文件放在项目仓库的根目录。
<div align=center>
    <img width = '20%' src ="https://images.gitee.com/uploads/images/2020/0325/212348_69716895_4840371.png "readme.png""/>
</div> 

<a name="cntitle"></a>
#### 四、实验结果
##### 1、lombok功能模块测试结果:
<div align=center>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/213248_6dfd7903_4840371.png "lombok运行结果.png""/>
</div>

##### 2、配置jetty作为Spring Boot应用程序的默认Servlet容器的结果:
#### `默认为tomcat`：
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/220545_0427ec2a_4840371.png "默认tomcat.png""/>
</div>  

#### `配置为jetty`：
<div align=center>
    <img  src ="https://images.gitee.com/uploads/images/2020/0325/220724_33d70496_4840371.png "证明是jetty.png""/>
</div>  

##### 3、简单的Spring Mvc控制器组件测试结果:
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/213448_3c68ff26_4840371.png "mvc测试结果.png""/>
</div>  

##### 4、CommandLineRunner的Bean运行结果（部分截图）:
<div align=center>
    <img width = '30%' src ="https://images.gitee.com/uploads/images/2020/0325/213653_63852737_4840371.png "commrunbean2结果.png""/>
</div>  

##### 5、简单的单元测试的测试结果:
#### `成功`：  
<div align=center>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/213831_55cb43bd_4840371.png "单元测试成功.png""/>
</div>  

#### `失败`：  
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/213923_3ddb77c7_4840371.png "单元测试失败.png""/>
</div>  

##### 6、使用IntelliJ IDEA的HTTP Client工具测试控制器端口测试结果:
#### `GET http://localhost:8080/`：  
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/214254_da67f1cf_4840371.png "http成功get.png""/>
</div>  

#### `POST http://localhost:8080/post`
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/214501_2e2f7baa_4840371.png "成功post.png""/>
</div>  

```
POST http://localhost:8080/Map
Content-Type: application/json

{
  "id": 999,
  "value": "content"
}
```
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/214748_35bd7ffb_4840371.png "成功Map类型.png""/>
</div>  

##### 7、在命令行中使用spring-boot插件运行Spring Boot应用程序，并把嵌入式Servlet容器的默认端口8080改为9090运行结果:  
##### `原来的端口为：8080`：
<div align=center>
    <img width = '90%' src ="https://images.gitee.com/uploads/images/2020/0325/215246_49411282_4840371.png "8080.png""/>
</div>  

##### `端口改为：9090`：
<div align=center>
    <img width = '90%' src ="https://images.gitee.com/uploads/images/2020/0325/215304_376d2cae_4840371.png "9090.png""/>
</div>  

##### 8、在属性文件中配置Spring Boot应用程序以debug模式运行结果（部分截图）:
<div align=center>
    <img width = '80%' src ="https://images.gitee.com/uploads/images/2020/0325/215646_62e53683_4840371.png "jardebug.png""/>
</div>

##### 9、在命令行中编译、打包Spring Boot应用程序的结果:
<div align=center>
    <img width = '50%' src ="https://images.gitee.com/uploads/images/2020/0325/215857_7c38c99b_4840371.png "打包pack.png""/>
</div>  

##### 10、在命令行中使用java命令运行Spring Boot应用程序的Jar文件运行结果（端口是8080）:
<div align=center>
    <img  src ="https://images.gitee.com/uploads/images/2020/0325/215938_089cdcec_4840371.png "jar原来8080.png""/>
</div>  

##### 11、在命令行中使用java命令运行Spring Boot应用程序的Jar文件，带参数改变嵌入式Servlet容器的默认端口8080改为9090运行结果:
<div align=center>
    <img  src ="https://images.gitee.com/uploads/images/2020/0325/220131_2112b9dc_4840371.png "jar9090.png""/>
</div>  

<a name="cntitle"></a>
#### 五、实验总结
```
1.本次实验通过Spring Initializr向导创建Spring Boot项目，是很便捷的方法，在构建的时候配置选择需要的starter即可，pom.xml会自动编写好，很方便。
2.本次实验让我们进一步掌握了解了spring boot的相关配置、测试、运用，也是对spring mvc、lombok的巩固。
3.通过这次实验我们也掌握了利用spring boot插件、java命令行运行、配置程序的方法，以及通过单元测试避免启动整个程序的方法
4.掌握Markdown轻量级语言编写README.md文件的方法。
```