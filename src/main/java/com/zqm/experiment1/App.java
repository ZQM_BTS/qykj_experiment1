package com.zqm.experiment1;

import lombok.Builder;
import lombok.Data;

public class App {
    public static void main(String[] args) {
        System.out.println("hello springboot!!");
        Student s = Student.builder().name("张琼敏Jonmin").age(21).build();
        System.out.println(s);
    }

    @Data
    @Builder
    static class Student {
        String name;
        int age;
    }
}
