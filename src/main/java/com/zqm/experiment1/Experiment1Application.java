package com.zqm.experiment1;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Map;


@SpringBootApplication
@RestController

public class Experiment1Application {

    public static void main(String[] args) {
        SpringApplication.run(Experiment1Application.class, args);
    }
    @RequestMapping("/")
    public String home() {
        return "这是一个简单的Spring Mvc控制器组件！";
    }
    @RequestMapping("/post")
    public String post() {
        return "这是post";
    }

    @PostMapping ("/Map")
    Map<String,Object> home(@RequestBody Map<String,Object>rq) {
        return rq;
    }

   @Bean
   public CommandLineRunner commandLineRunner3 (ApplicationContext ctx) {
       return args -> {
           System.out.println("由Spring Boot注册的所有Bean: ");
           String[] beanNames = ctx . getBeanDefinitionNames();
           Arrays. sort(beanNames);
           for (String beanName : beanNames) {
               System.out.println(beanName);
           }
       };
   }
// 有多个CommandLineRunner实现类且需按顺序输出的时候
//    @Order(value=3)
//    @Component
//    class ApplicationStartupRunnerThree implements CommandLineRunner {
//        protected final Log logger = LogFactory.getLog(getClass());
//        @Override
//        public void run(String... args) throws Exception {
//            logger.info("由Spring Boot注册的所有Bean:3 !!");
//        }
//    }
//
//    @Order(value=2)
//    @Component
//    class ApplicationStartupRunnerTwo implements CommandLineRunner {
//        protected final Log logger = LogFactory.getLog(getClass());
//        @Override
//        public void run(String... args) throws Exception {
//            logger.info("由Spring Boot注册的所有Bean:2 !!");
//        }
//    }
//
//    @Order(value=1)
//    @Component
//    class ApplicationStartupRunnerOne implements CommandLineRunner {
//       protected final Log logger = LogFactory.getLog(getClass());
//        @Override
//        public void run(String... args) throws Exception {
//            logger.info("由Spring Boot注册的所有Bean:1 !!");
//        }
//    }


}
